# Dev Seminars

## Install

- Clone this repo
- Run `yarn` to install dependencies

## Initial Repo Setup (reference only)

This section is for reference only and doesn't need to be repeated unless you are wanting to create a similar project from scratch.

This repo was initially created using `yarn` and `create-react-app`

`yarn create react-app devseminars --template typescript`

In `package.json` I pinned `react` + `react-dom` versions to 17.0.2

### Additional Installs

Material UI
`yarn add @mui/material @emotion/react @emotion/styled`

Fonts
`yarn add @fontsource/nunito-sans`

Storybook
`npx sb init`

MobX

- `yarn add mobx mobx-react-lite`
- Set `useDefineForClassFields` to true in `tsconfig.json`
