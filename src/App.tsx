import React from 'react';
import { styled } from '@mui/material/styles';

import '@fontsource/nunito-sans';

const StyledRoot = styled('div', { name: 'App' })({
  fontFamily: 'Nunito Sans',
});

export interface AppProps {
  className?: string;
}

const App: React.FC<AppProps> = ({ className }) => {
  return <StyledRoot className={className}>App</StyledRoot>;
};

export default App;
